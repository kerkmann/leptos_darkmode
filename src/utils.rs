/*
* The MIT License (MIT)
*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

use leptos::window;
use web_sys::Storage;

const LOCALSTORAGE_KEY: &str = "darkmode";

/// Retrieves the local storage object, if available.
pub(crate) fn get_storage() -> Option<Storage> {
    window().local_storage().ok().flatten()
}

/// Retrieves the dark mode state from local storage, if available.
pub(crate) fn storage_get_state() -> Option<bool> {
    get_storage()
        .and_then(|storage| storage.get(LOCALSTORAGE_KEY).ok())
        .flatten()
        .and_then(|entry| entry.parse::<bool>().ok())
}

/// Checks whether the user's system prefers dark mode based on media queries.
pub(crate) fn prefers_darkmode() -> bool {
    window()
        .match_media("(prefers-color-scheme: dark)")
        .ok()
        .flatten()
        .map(|media| media.matches())
        .unwrap_or_default()
}

/// Stores the dark mode state in local storage.
pub(crate) fn storage_set_state(state: bool) {
    if let Some(storage) = get_storage() {
        storage
            .set(LOCALSTORAGE_KEY, state.to_string().as_str())
            .ok();
    }
}
