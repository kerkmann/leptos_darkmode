/*
* The MIT License (MIT)
*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

use leptos::{
    create_effect, expect_context, provide_context, RwSignal, SignalGet, SignalSet, SignalUpdate,
};
use utils::{prefers_darkmode, storage_get_state, storage_set_state};

mod utils;

#[derive(Debug, Clone)]
pub struct Darkmode {
    state: RwSignal<bool>,
}

impl Darkmode {
    /// Initializes a new Darkmode instance.
    ///
    /// This function initializes a new `Darkmode` instance, sets the initial mode based on local storage (if available).
    ///
    /// Returns a `Darkmode` instance.
    #[must_use]
    pub fn init() -> Self {
        let darkmode = Self {
            state: RwSignal::new(false),
        };

        provide_context(darkmode);

        create_effect({
            let darkmode = expect_context::<Darkmode>();
            move |_| {
                let initial = storage_get_state().unwrap_or(prefers_darkmode());

                if initial {
                    darkmode.state.set(true);
                }
            }
        });

        expect_context::<Darkmode>()
    }

    /// Toggles between dark and light modes.
    pub fn toggle(&mut self) {
        self.state.update(|state| {
            *state = !*state;
            storage_set_state(*state);
        });
    }

    /// Sets the mode to dark.
    pub fn set_dark(&mut self) {
        self.set(true);
    }

    /// Sets the mode to light.
    pub fn set_light(&mut self) {
        self.set(false);
    }

    /// Sets the mode explicitly to either dark or light.
    ///
    /// - `dark`: Set to `true` for dark mode, and `false` for light mode.
    pub fn set(&mut self, dark: bool) {
        self.state.set(dark);
        storage_set_state(dark);
    }

    /// Returns the current mode (dark or light).
    #[must_use]
    pub fn get(&self) -> bool {
        self.state.get()
    }

    /// Returns whether the current mode is dark.
    #[must_use]
    pub fn is_dark(&self) -> bool {
        self.state.get()
    }

    /// Returns whether the current mode is light.
    #[must_use]
    pub fn is_light(&self) -> bool {
        self.state.get()
    }
}
