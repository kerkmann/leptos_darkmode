# Leptos Darkmode Helper

The [Tailwind CSS](https://tailwindcss.com) Darkmode Helper is a Rust crate
that provides a convenient tool for managing dark mode functionality in web
applications built with Leptos. It allows you to easily set and get the dark
mode state, and persist this state in the local storage of a web browser.

## Table of Contents

- [Leptos compatibility](leptos-compatibility)
- [Installation](#installation)
- [Usage](#usage)
  - [Initialization](#initialization)
  - [Toggling Dark Mode](#toggling-dark-mode)
  - [Setting Dark and Light Modes](#setting-dark-and-light-modes)
  - [Getting the Current Mode](#getting-the-current-mode)
- [License](#license)


## Leptos compatibility

| Crate version | Compatible Leptos version |
|---------------|---------------------------|
| <= 0.1        | 0.5                       |
| 0.2           | 0.6                       |

## Installation

Add the Darkmode Helper crate to your `Cargo.toml` file:

```toml
[dependencies]
leptos_darkmode = "0.2"
```

## Usage

In your Leptos-based project, you can use the Darkmode Helper to manage dark
mode functionality easily. This is using the
[Tailwind CSS class strategy](https://tailwindcss.com/docs/dark-mode#toggling-dark-mode-manually),
instead of the media strategy. This needs to be enabled in the `tailwind.config.js`:
```js
/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  // ...
}
```
After that you can toggle the `dark` class in the `html`, as you can see below.

### Initialization

To get started, you need to initialize the Darkmode Helper. This should be done
only once in your application. The `init` function will read the current dark
mode state from local storage if it exists; otherwise, it will determine the
default state based on the user's system preference.

```rust
#[component]
pub fn App() -> impl IntoView {
    provide_meta_context();

    let darkmode = Darkmode::init();
    // The dark mode state is now initialized and set.

    view! {
        <Stylesheet id="leptos" href="/pkg/main.css"/>

        <Link rel="shortcut icon" type_="image/ico" href="/favicon.ico"/>

        // This set's the darkmode tag on the html class, if the darkmode is enabled
        <Html lang="en" class=move || if darkmode.is_dark() { "dark" } else { "" }/>

        // Set your tailwindcss theme, for example the background color of the body
        <Body class="bg-white dark:bg-gray-800"/>

        // Your router code
    }
}
```
### Toggling Dark Mode

You can easily toggle the dark mode state using the `toggle` method. This will
switch between dark and light modes and update the state in local storage for
persistence.

```rust
#[component]
fn navbar() {
    let mut darkmode = expect_context::<Darkmode>();

    view! {
        // Toggle the dark mode state
        <button on:click=move |_| darkmode.toggle()>Toggle Darkmode</button>
    }
}
```

### Setting Dark and Light Modes

If you want to explicitly set the dark or light mode, you can use the `set_dark`
and `set_light` methods. These methods will update the state and persist it in
local storage.

```rust
#[component]
fn settings() {
    let mut darkmode = expect_context::<Darkmode>();

    view! {
        // Set dark mode explicitly
        <button on:click=move |_| darkmode.set_dark()>Set Darkmode</button>

        // Set light mode explicitly
        <button on:click=move |_| darkmode.set_light()>Set Lightmode</button>
    }
}
```

### Getting the Current Mode

You can check the current mode using the `get`, `is_dark`, or `is_light`
methods. These methods return `true` for dark mode and `false` for light mode.

```rust
fn main() {
    let darkmode = expect_context::<Darkmode>();

    // Check if it's dark mode
    let is_dark_mode = darkmode.is_dark();

    // Check if it's light mode
    let is_light_mode = darkmode.is_light();
}
```

## License

`leptos_darkmode` is distributed under the [MIT License](https://opensource.org/licenses/MIT).
For more information, see the [LICENSE](https://gitlab.com/kerkmann/leptos_darkmode/blob/main/LICENSE) file.
